#!/usr/bin/env python
#coding:utf8

from selenium import webdriver
import unittest
import os, time

class TestCookies(unittest.TestCase):
	def setUp(self):
		chromedriver = "drivers/chromedriver_x64"
		os.environ["webdriver.chrome.driver"] = chromedriver
		options = webdriver.ChromeOptions()
		options.add_argument("--user-data-dir=chrome")
		self.driver = webdriver.Chrome(executable_path=chromedriver,chrome_options=options)
		self.driver.implicitly_wait(30)

	def test_01_cookieWithConcreteDomain(self):
		print "Установка и считывание cookie для конкретного домена"
		driver = self.driver
		driver.get("http://my.local.ru")
		# Куки на конкретный домен
		driver.add_cookie({
			'name':'foo', 'value':'bar',
			'domain':'my.local.ru'
		})
		# Читаются на том-же домене
		self.assertIsNotNone( driver.get_cookie('foo') )
		self.assertEqual('bar',driver.get_cookie('foo')['value'] )
		# Не читаются на другом домене
		driver.get("http://local.ru")
		self.assertIsNone( driver.get_cookie('foo') )

	def test_02_cookieWithSubDomain(self):
		print "Установка и считывания cookie для поддоменов"
		driver = self.driver
		driver.get("http://my.local.ru")
		# Куки, общий для домена и поддоменов
		driver.add_cookie({
			'name':'foo', 'value':'bar',
			'domain':'.local.ru'
		})
		# Читаются на родственном домене
		driver.get("http://local.ru")
		self.assertIsNotNone( driver.get_cookie('foo') )
		self.assertEqual('bar',driver.get_cookie('foo')['value'] )

	def test_03_cookieWithExpiry(self):
		print "Установка и считывание cookie с ограничением по времени"
		driver = self.driver
		driver.get("http://my.local.ru")
		# Свежайшие куки, но маленький срок хранения
		driver.add_cookie({
			'name':'foo', 'value':'bar',
			'domain':'.local.ru',"path":"/",
			'expiry': int(time.time()) + 2
		})
		# Через пару секунд на печенье появляется плесень...
		time.sleep(4)
		self.assertIsNone( driver.get_cookie('foo') )

	def test_04_setCookieThroughJS(self):
		print "Установка cookie через JavaScript"
		driver = self.driver
		driver.get("http://my.local.ru")
		# Установка кук через JS
		driver.execute_script('document.cookie="alpha=beta"')
		self.assertIsNotNone( driver.get_cookie('alpha') )
		self.assertEqual('beta',driver.get_cookie('alpha')['value'] )

	def test_05_cookieWithPath(self):
		print "Установка и считывание cookie для пути в домене"
		driver = self.driver
		driver.get("http://my.local.ru")
		# Установка куки для пути '/adverts' на сайте.
		driver.add_cookie({
			'name':'foo', 'value':'bar',
			'domain':'my.local.ru',
			'path':'/adverts'
		})
		# По другому пути видно быть не должно
		self.assertIsNone( driver.get_cookie('foo') )
		driver.get("http://my.local.ru/adverts")
		# А по соответствующему пути должно быть видно
		self.assertIsNotNone( driver.get_cookie('foo') )
		self.assertEqual('bar',driver.get_cookie('foo')['value'] )

	def test_06_enormousCookieSize(self):
		print "Установка куки разных размеров"
		driver = self.driver
		driver.get("http://my.local.ru")
		# Максимальный размер куки в Chrome (по документам) - 4096, на деле больше 4064 не принимает
		enormous = 'Y'*4064
		driver.add_cookie({ 'name':'foo', 'value':enormous })
		self.assertIsNotNone( driver.get_cookie('foo') )
		self.assertEqual(enormous,driver.get_cookie('foo')['value'] )
		driver.delete_cookie('foo')
		# Недопустимый размер
		enormous = 'Y'*5000
		driver.add_cookie({ 'name':'foo', 'value':enormous })
		self.assertIsNone( driver.get_cookie('foo') )

	def test_07_cookieInTheFrame(self):
		print "Установка и считывание кук во фрейме"
		driver = self.driver
		driver.get("http://my.local.ru")
		driver.add_cookie({ 'name':'foo', 'value':'bar', 'domain':'.local.ru' })
		# Переключаемся на фрейм, читаем куки там
		driver.switch_to_frame("InnerFrame")
		self.assertIsNotNone( driver.get_cookie('foo') )
		self.assertEqual('bar',driver.get_cookie('foo')['value'] )
		# Меняем куки во фрейме
		driver.add_cookie({ 'name':'foo', 'value':'frameBar', 'domain':'.local.ru' })
		# В основном окне куки изменились соответственно
		driver.switch_to_default_content()
		self.assertIsNotNone( driver.get_cookie('foo') )
		self.assertEqual( 'frameBar', driver.get_cookie('foo')['value'] )

	def tearDown(self):
		self.driver.quit()

if __name__ == "__main__":
	unittest.main()